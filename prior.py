from it_discrete.constants import *
import it_discrete.loss
import it_discrete.util
import itertools
from it_discrete.input import InputGenerator
import sys, time
from it_discrete.pdf import Pdf2

class Prior(object):
  """
  Stores prior probability of a given x being most likely to produce correct output.
  """
  def __init__(self, inputGen, pdf, capturing=True, entropyBias=0.005):
    self.pdf = pdf
    self.inputGen = inputGen
    self.capturing = capturing
    self.entropyBias = entropyBias

    self._priorArray = np.zeros(len(self.inputGen.getInputs()))
    self._starIndicesToIndex = {}
    xStars = self.inputGen.getInputs()
    for i, xStar in enumerate(xStars):
      self._starIndicesToIndex[xStar] = i
    self.reset()

  def reset(self):
    # Initially, populate the prior so all targets are equally likely
    for i,inputIndices in enumerate(self.inputGen.getInputs()):
      self._priorArray[i] = 1.0
    self._normalize()

  def getEntropy(self):
    priorArray = self._priorArray # array indexed by xStarIndex
    nonzero = priorArray != 0.0
    priors = priorArray[nonzero]
    return np.sum(-1.0 * priors * np.log(priors))

  def getExpectedOutputEntropy(self, x):
    """
    Gets entropy of distribution p(y|x)
    """
    entropy = 0.0

    # We have to sum over all y (weighted by probability of seeing that y given x)
    for y in it_discrete.loss.Y_INDICES:
      
      # We have to calculate P(y|x), the probability of seeing an output y for the given x
      pY = 0.0 # Prob seeing y given x
      for xStar in self.inputGen.getInputs():
        pXStar = self.priorProb(xStar) # Prob target is xStar
        pYCond = self.pdf.p(x, xStar, y) # Prob seeing y given x, xStar
        pY += pXStar * pYCond

      if pY == 0.0:
        continue

      entropy += -1.0 * pY * np.log(pY)

    return entropy

  def getExpectedEntropy(self, x):
    """
    Returns the expectation of the entropy for a proposed move x.
    """
    nY = it_discrete.loss.NUM_LOSS_BINS

    probMatrix = self.pdf._probMap[x] # array indexed by [yBin, xStarIndex]
    priorArray = self._priorArray # array indexed by xStarIndex
    probY = np.sum(probMatrix * priorArray, axis=1, keepdims=True) # returns column vector of probY for each Y

    nonzero = probY > 0.0
    nYNZ = np.sum(nonzero)
    probYnonzero = probY[nonzero].reshape(nYNZ, 1)
    pXStarCond = (probMatrix[nonzero.reshape(nY),:] * priorArray) / probYnonzero
    
    # Add very small number so that x*log(x) evaluates to near zero when x is zero (instead of raising exception)
    pXStarCond += 0.00000000001
    entropy = np.sum(-1.0 * probYnonzero * pXStarCond * np.log(pXStarCond))
    
    entropy += self.entropyBias

    if self.capturing:
      entropy *= (1-self.priorProb(x))
    
    return entropy

  def getExpectedEntropySlow(self, x):
    entropy = 0.0

    # We have to sum over all y (weighted by probability of seeing that y given x)
    for y in it_discrete.loss.Y_INDICES:
      
      # We have to calculate P(y|x), the probability of seeing an output y for the given x
      pY = 0.0 # Prob seeing y given x
      for xStar in self.inputGen.getInputs():
        pXStar = self.priorProb(xStar) # Prob target is xStar
        pYCond = self.pdf.p(x, xStar, y) # Prob seeing y given x, xStar
        pY += pXStar * pYCond

      if pY == 0.0:
        continue

      # We have to calculate the new value of the prior for each xStar
      for xStar in self.inputGen.getInputs():
        pXStar = self.priorProb(xStar)
        pYCond = self.pdf.p(x, xStar, y)
        pXStarCond = pXStar * pYCond / pY

        if pXStarCond == 0.0:
          continue

        entropy += -1.0 * pY * pXStarCond * np.log(pXStarCond)
    
    entropy += self.entropyBias

    # When capturing is on, we stop on hitting target (which sends entropy to zero)
    if self.capturing:
      entropy *= (1-self.priorProb(x))

    return entropy

  def updatePrior(self, x, y):
    # First have to calculate P(y|x), the probability of seeing an output y for the given x
    probMatrix = self.pdf._probMap[x] # array indexed by [yBin, xStarIndex]
    priorArray = self._priorArray # array indexed by xStarIndex
    pY = np.sum(probMatrix[y,:] * priorArray)

    # Check that prob is nonzero
    if pY == 0.0:
      raise ProbException('Encountered a y value that seems impossible based on empirical PDF (x=%s, y=%d)' % (str(x), y))

    self._priorArray *= (probMatrix[y,:]/pY)
    self._normalize()

  def priorProb(self, xStarInds):
    """
    Returns the prior probability of xStarInds being the correct target sequence.

    This gets updated in each step of infotaxis.
    """
    return self._priorArray[self._starIndicesToIndex[xStarInds]]

  def _normalize(self):
    totalP = np.sum(self._priorArray)
    self._priorArray /= totalP

class ProbException(Exception):
  pass

if __name__ == '__main__':
  runNum = int(sys.argv[1])
  inputGen = InputGenerator(True)
  pdf = Pdf2.loadFromFile(it_discrete.util.getPdfFile(runNum), True)
  prior = Prior(inputGen, pdf, capturing=False)

  # print(len(list(c2)))
  # sys.exit(0)
  print(c1)
  print(c2)
  sys.exit(0)
  diffs = []
  for a, b in zip(c1, c2):
    #v1 = a[0] * a[1] * np.log(a[1])
    #v2 = b[0] * b[1] * np.log(b[1])
    print(a - b)
    #print(a-b)
    diffs.append(a-b)

  print(sum(diffs))

