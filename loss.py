import numpy as np

# We bin loss to make problem discrete
MAX_LOSS = 1.0
NUM_LOSS_BINS = 20
LOSS_RIGHT_EDGES = [(MAX_LOSS/NUM_LOSS_BINS) * (i+1) for i in range(NUM_LOSS_BINS)]
Y_INDICES = list(range(NUM_LOSS_BINS))


def loss(templateMotors, motors):
  error = 0.0
  for template, motor in zip(templateMotors, motors):
    error += (np.linalg.norm(template - motor)**2)
  return error

def lossMany(templateMotors, motorsArray):
  errors = np.zeros(len(motorsArray))
  for i,motor in enumerate(motorsArray):
    diff = templateMotors - motor
    errors[i] = sum(sum(diff * diff))
  return errors

def getLossBin(loss):
  for i,rightEdge in enumerate(LOSS_RIGHT_EDGES):
    if loss < rightEdge:
      return i
  raise Exception('Loss value exceeds max: %f > %f' % (loss, MAX_LOSS))
