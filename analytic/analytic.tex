\documentclass{article}
\usepackage{sjbsymbols}
\usepackage{sjbdoc}


\begin{document}

Let $\b x$ be the input spike time array and $\b y$ be the outputted error function. For a bird trying to learn a song $\b m^*$, we have:
\[
  \tn{(input spikes)}\quad
  X
  \quad
  \overset{\tn{motor noise}}{\to}
  \quad
  X'
  \quad
  \overset{\tn{A}}{\to}
  \quad
  M
  \quad
  \overset{\tn{sensory noise}}{\to}
  \quad
  M'
  \quad
  \overset{\tn{error against $\b m^*$}}{\to}
  \quad
  Y
  \quad\tn{(output error)}
\]

Let $\b x^*$ be the input spike that minimizes the output error $|\b y|$. The entropy of the distribution comes from our uncertainty on $\b x^*$. Thus, we work with the prior $P_t(\b x^*)$, our guess at time $t$ at the probability of the optimal input $\b x^*$
\sp

\b{Bayesian Update}
\stlst
  \item In timestep $t$, have prior on optimal input $P_t(\b x^*)$
  \item Try a trial spike train $\b x_t$ and get corresponding output $\b y_t(\b x_t)$. The method by which we choose $\b x_t$ is our {\bf policy}. The infotaxis policy is defined below.
  \item Use the output we received to update our prior:
  \[
    P_{t+\d t}(\b x^*) 
    \equiv 
    P_t(\b x^*|\b y_t(\b x_t))
    =
    \f{P_t(\b x^*, \b y_t(\b x_t))}{P_t(\b y_t(\b x_t))}
    =
    \f{P(\b y_t(\b x_t)|\b x^*)P_t(\b x^*)}{\sum_{\b x^{*'}} P(\b y_t(\b x_t)|\b x^{*'})
    P_t(\b x^{*'})}
  \]
\edlst
Here $P_t(\b x^*|\b y_t(\b x_t))$ is the probability that the optimal input is $\b x^*$ given that when we tried input $\b x_t$ we got $\b y_t$ as our output. 
In above formula we have two pieces, $P_t(\b x^*)$ and $P(\b y_t(\b x_t)|\b x^*)$. 
The former is iteratively updated in each step so we don't need to calculate it before the learning process. We only need to supply the initial prior $P_0(\b x^*)$.
The latter expression, $P(\b y_t(\b x_t)|\b x^*)$ is independent of our prior and {\bf needs to be computed numerically before learning begins.}
\sp
%
%
%

{\bf Infotaxis movement policy}
\stlst
\item In timestep $t$, have prior $P_t(\b x^*)$ which yields entropy:
  \[
    S_t = -\sum_{\b x^*}P_t(\b x^*)\log(P_t(\b x^*))
  \]
\item Suppose our previous trial spike train was $\b x_{t-1}$. There is a set of allowed trial spikes we are allowed to move to in this step which we denote as $X_t$. These are spike trains $\b x_t$ that are nearby the previous trial $x_{t-1}$.
\item If we move to $x_t$, our expected new entropy is:
\[
  S_{t+1} = -\sum_{\b x^*} P_{t+\d t}(\b x^*)\log P_{t+\d t}(\b x^*)
  =
  -\sum_{\b x^*}\l\{
  \f{P(\b y_t(\b x_t)|\b x^*)P_t(\b x^*)}{\sum_{\b x^{*'}} P(\b y_t(\b x_t)|\b x^{*'})P_t(\b x^{*'})}
  \log\l[
    \f{P(\b y_t(\b x_t)|\b x^*)P_t(\b x^*)}{\sum_{\b x^{*'}} P(\b y_t(\b x_t)|\b x^{*'})P_t(\b x^{*'})}
  \r]\r\}
\]
\item The challenge is that we want to choose $x_{t}\in X_t$ such that $S_{t+1}$ is as small as possible
\edlst

% \b{Concrete Steps}
% \stlst
% \item Need to write function that gives error $\b y_t$ for motor output $\b m$ given that target song is $\b m^*$.
% \item We need to know $P(\b y_t(\b x_t)|\b x^*)$. 
% This is the probability that an input $\b x_t$ yields an output $\b y_t$ given that $\b x^*$ is the song the bird is trying to learn.
% The value of this function must be computed numerically with monte carlo. Its value will be sensitive to our choice of noise functions.
% \item We need to define precisely what $X_t$ is: the set of trial spike trains $x_t$ we are allowed to try at time $t$ given that at time $t-1$ we used $x_{t-1}$
% \item We need to find a computationally tractable way of finding $x_t\in X_t$ that minimizes $S_{t+1}$
% \item It would be nice to try to understand $p(\b y_t(\b x_t)|\b x^*)$ analytically
% \item It would also be nice to show that this policy eventually leads to choosing $\b x_t$ that minimizes $\b y_t$ (i.e. the bird sings correctly)
% \item We should figure out a way to choose a reinforcement learning policy given the model described above.
% \edlst

% {\bf Alternate approaches}
% We wrote prior as a probability on the correct song $P(\b x^*)$. This assumes the birds internal machinery (the matrix $A$, the motor/sensory noise) is fixed and intrinsically known to the bird (since $P(y(x)|x^*)$ is a function of these factors). We could alternatively make the song, the noise and the $A$ mapping unknown, but this would drastically increase our search space.
% \sp

% We could also make the prior a function of $\b x_0$, the spike train which minimizes the expected error. The problem is that for any set of parameters, this is never unique. There are many spike trains $x$ that will minimize $y_t(x)$
\newpage\noindent
Prior update. After trying $x$ and getting $y$:
\[
  P_{t+\d t}(x^*) 
  =
    \f{P(y|x, x^*)P_t(x^*)}{\sum_{\b x^{*'}} P(y|x,x^{*'})
    P_t(x^{*'})}
\]
New entropy:
\[
  \<S_{t+1}(x)\>
    = \sum_{x^*}\sum_y P(y|x,x^*)
    \f{P(y|x, x^*)P_t(x^*)}{\sum_{\b x^{*'}} P(y|x,x^{*'})
    P_t(x^{*'})}
    \log\l\{
    \f{P(y|x, x^*)P_t(x^*)}{\sum_{\b x^{*'}} P(y|x,x^{*'})
    P_t(x^{*'})}
    \r\}
\]
\end{document}