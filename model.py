import it_discrete.loss
import it_discrete.noise
import it_discrete.motor
from it_discrete.constants import *
import itertools

MATRIX_A = np.array([
  [ 0.0,          1.86760957,  0.0,         -2.01545564],
  [ 3.61140127,  0.0,          1.65827626,  0.0        ]
])
# MATRIX_A = np.array([
#   [ 0.        ,  0.        , -3.83171748,  1.75437083],
#   [ 2.86206806,  0.11810068,  0.        ,  0.        ]
# ])

class Model(object):
  """
  Represents a specific instance of songbird learning model
  """
  def __init__(self, xStarIndices, matrixA=MATRIX_A):
    self.xStarIndices = xStarIndices
    self.xStarTimes = timeIndicesToTimes(xStarIndices)
    self.matrixA = matrixA
    self.templateMotors, _ = self.getMotors(self.xStarIndices, noise=False)

  def getMotors(self, xIndices, noise=True):
    # Convert indices to times
    xTimes = timeIndicesToTimes(xIndices)

    # Add spike time noise
    if noise:
      xTimes = xTimes + np.random.normal(scale=SPIKE_NOISE_SIGMA, size=len(xIndices))

    # Convert spike times to motor signal
    motors, times = it_discrete.motor.getMotorSignal(xTimes, self.matrixA)

    return motors, times

  def getLossBin(self, xIndices, noise=True):
    loss = self.getLoss(xIndices, noise=noise)
    return it_discrete.loss.getLossBin(loss)

  def getLoss(self, xIndices, noise=True):
    """
    Computes error for given input signal and returns its value rounded to nearest bin edge (kind of).
    """
    motors,_ = self.getMotors(xIndices, noise=noise)
    return it_discrete.loss.loss(self.templateMotors, motors)

  def getMotorsTrials(self, xIndices, num):
    # Convert indices to times
    xTimes = timeIndicesToTimes(xIndices)

    trials = np.random.normal(scale=SPIKE_NOISE_SIGMA, size=(num, len(xIndices))) + xTimes
    # Convert spike times to motor signal
    motors = it_discrete.motor.getMotorSignalTrials(trials, self.matrixA)

    return motors

  def getLossTrials(self, xIndices, num):
      """
      Computes error for each input given in inputs
      """
      motors = self.getMotorsTrials(xIndices, num)
      return it_discrete.loss.lossMany(self.templateMotors, motors)

def generateMatrixARandomly(nRAs=NUM_RA_NEURONS, nMotors=NUM_MOTORS):
    """generates weight matrix for mapping from RA firing to M input, following Duffy et al. (PNAS, 2019) paper
    random perturbation will be the same for same random seed
    nRAs is the number of RA neurons"""

    # We want number of RA neurons to be divisible by num motor pools so we can
    # evenly partition the RA neurons into which motor pool they map too.
    if nRAs % nMotors != 0:
        raise Exception('nRAs must be divisible by nMotors')

    if nMotors != 2:
        raise Exception('nMotors not implemented except for n=2')
    indices_of_ra_mapping_to_m1 = list(np.random.choice(nRAs, size=nRAs//2, replace=False))
    indices_of_ra_mapping_to_m1_inhibitory = list(np.random.choice(indices_of_ra_mapping_to_m1, size=len(indices_of_ra_mapping_to_m1)//2, replace=False))
    A = np.zeros((2, nRAs))
    for RAneuron in range(nRAs):
        abs_val = np.random.uniform(0, 4)
        if RAneuron in indices_of_ra_mapping_to_m1 and RAneuron in indices_of_ra_mapping_to_m1_inhibitory:
            A[0, RAneuron] = -abs_val
        elif RAneuron in indices_of_ra_mapping_to_m1 and not RAneuron in indices_of_ra_mapping_to_m1_inhibitory:
            A[0, RAneuron] = abs_val
        elif not RAneuron in indices_of_ra_mapping_to_m1:
            A[1, RAneuron] = abs_val
    return A

if __name__ == '__main__':
  print(generateMatrixARandomly())
