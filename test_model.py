
import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt
import os

import it_discrete.loss as loss
import it_discrete.motor, it_discrete.noise, it_discrete.model, it_discrete.util
from it_discrete.constants import *


def saveSpikeTrainPlot(spikeTimes, tMax=T_MAX, outputDir=FIG_DIR, plotName='spike_train.png'):
    """
    For purposes of plot, each "spike" is a narrow gaussian.

    Note: For high numbers of RA (e.g. 480), this plot is pretty useless.
    """
    sigma = tMax / 10000.0 # width of each spike
    plt.clf()
    plt.xlim(0, tMax)
    for t in spikeTimes:
        times = np.linspace(t - 3*sigma, t + 3*sigma, 100)
        plt.plot(times, norm.pdf(times, t, sigma), color='blue')
        plt.xlabel('Time')
        plt.ylabel('RA Spikes')
    plt.savefig(os.path.join(outputDir, plotName))


def saveMotorPlot(times, motors, outputDir=FIG_DIR, suffix=''):
    nMotors = len(motors[0])
    for m in range(nMotors):
        plt.clf()
        plt.plot(times, motors[:,m])
        plt.xlabel('Time')
        plt.ylabel('Motor %d output' % m)
        plt.savefig(os.path.join(outputDir, 'motor%d%s.png' % (m, suffix)))


#x = it_discrete.model.Model.generateRandomInput()
xStar = (0, 1, 2, 3)
#x = (0, 1, 2, 3)
x = (4,5, 6, 7)
#xStar = it_discrete.model.Model.generateRandomInput()
#x = tuple(xStar)
m = it_discrete.model.Model(xStar)

xTimes = timeIndicesToTimes(x)
xTimes += np.random.normal(scale=SPIKE_NOISE_SIGMA, size=len(x))

motors, times = m.getMotors(x, noise=True)
motorsS, times = m.getMotors(xStar, noise=False)

saveSpikeTrainPlot(timeIndicesToTimes(x), plotName='spikes_input.png')
saveSpikeTrainPlot(xTimes, plotName='spikes_input_noise.png')
saveSpikeTrainPlot(timeIndicesToTimes(xStar), plotName='spikes_template.png')
it_discrete.util.savePlots(times, motors, 'motor%d_input', xlabel='Time')
it_discrete.util.savePlots(times, motorsS, 'motor%d_template', xlabel='Time')

plt.clf()
plt.plot(times, motors[0], color='blue')
plt.plot(times, motorsS[0], color='red')
plt.savefig(os.path.join(FIG_DIR, 'motors.png'))


plt.clf()
plt.plot(times, motors[0]-motorsS[0], color='blue')
plt.savefig(os.path.join(FIG_DIR, 'motor_diff.png'))


dt = times[1] - times[0]
error = 0.0
for mi in [0, 1]:
  diff = motors[mi] - motorsS[mi]
  for t in range(len(times)-1):
    error += (diff[t+1] * dt)**2

diff0 = motors[0] - motorsS[0]
diff1 = motors[1] - motorsS[1]
print('e1: %f' % error)
print('e2: %f' % ((diff0 @ diff0) + (diff1 @ diff1)))
print('e3: %f' % loss.loss(motorsS, motors))

error4 = 0.0
for i in [0, 1]:
  error4 += (np.linalg.norm(motorsS[i]-motors[i]))
print('e4: %f' % error4)
