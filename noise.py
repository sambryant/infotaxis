import numpy as np
from scipy.ndimage import gaussian_filter
import os
from it_discrete.constants import *

OUTPUT_DIR=os.path.join(FIG_DIR, 'noise_tests')

def integrateSignal(signal, dt=DT):
  """
  Returns integrated signal. Not optimized.
  """
  newSignal = np.zeros(len(signal))
  value = 0.0
  for i,s in enumerate(signal):
    value = value + s*dt
    newSignal[i] = value
  return newSignal

def gaussianConvolution(signal, sigma, dt=DT):
  """
  Returns signal convoluted with a Gaussian kernel

  Signal can be of arbitrary dimension.

  signal: array of values of signal for given times.
  sigma: the std deviation of the gaussian kernel in seconds.
  """
  # We have to convert the sigma from units of time to units of steps.
  sigmaSteps = sigma/dt

  return gaussian_filter(signal, sigma=sigmaSteps)

def squareConvolution(times, signal, winsize):
  """
  Returns signal convoluted with a square window of given size.
  """
  newSignal = np.zeros(len(signal))

  for i in range(len(signal)):
    indices = np.abs(times-times[i]) < (winsize/2)
    newSignal[i] = np.average(signal[indices])
  return newSignal

def generateWhiteNoiseTrace(sigma, nSteps, nTraces=1, dt=DT):
  """
  Warning: use of sigma might be off by factor of two.

  sigma: std deviation of white noise signal.
  nSteps: number of time steps in the signal (length of returned array)
  nTraces: if more than 1, will return matrix where each row is a different trace.
  dt: time step used to compute trace.
  """
  t = 0.0
  x = np.zeros(nTraces)
  xs = np.zeros((nTraces, nSteps))
  ts = np.zeros(nSteps)
  for i in range(nSteps):
    xs[:,i] = x[:]
    ts[i] = t
    x[:] += np.sqrt(dt) * sigma * np.random.randn(nTraces)
    t += dt

  if nTraces == 1:
    return ts, xs[0,:]
  else:
    return ts, xs
