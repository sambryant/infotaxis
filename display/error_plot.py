from it_discrete.constants import *
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation
import numpy as np
import it_discrete.loss
import it_discrete.util
from it_discrete.input import InputGenerator
import it_discrete.model
from scipy.stats import norm
import sys, os
plt.style.use(['./mystyle.mplstyle'])

PLOT_OFFSET = 0.5
SPIKE_V_OFFSET = 2.2
TARGET_COLOR = 'black'
TRIAL_COLOR = 'tab:green'
POST_COLOR = 'tab:blue'
ENTR_COLOR = 'tab:olive'


def printRunInfo(outputName):
  f = np.load(outputName, allow_pickle=True)

  xStar = f['xStar']
  nSteps = f['nSteps'].item()
  trialInputs = f['trialInputs']
  trialOutputs = f['trialOutputs']
  entropyMaps = f['entropyMaps']
  priorMaps = f['priorMaps']

  capturing = f['capturing'].item()
  nearestNeighbor = f['nearestNeighbor'].item()
  ordering = f['ordering'].item()
  randomMoves = f['randomMoves'].item()

  print('Infotaxis learning run information:')
  print('\txStar: %s' % str(xStar))
  print('\tnSteps: %d' % nSteps)
  print('\tcapturing: %s' % str(capturing))
  print('\tnearest neighbor: %s' % str(nearestNeighbor))
  print('\tordering: %s' % str(ordering))
  print('\trandom moves: %s' % str(randomMoves))

def errorPlot(outputName):
  f = np.load(outputName, allow_pickle=True)

  xStar = f['xStar']
  nSteps = f['nSteps'].item()
  trialInputs = f['trialInputs']
  trialOutputs = f['trialOutputs']

  # Compute mean error for correct target
  m = it_discrete.model.Model(xStar)
  errors = m.getLossTrials(xStar, 500)
  minError = np.average(errors)
  
  plt.plot(trialOutputs, color=TRIAL_COLOR, label='trial error')
  plt.plot([minError for _ in range(len(trialOutputs))], color=TARGET_COLOR, label='min error')
  plt.show()

def makeAveragedErrorPlot(runNum, subdir, saveName=None):
  d = os.path.join(it_discrete.util.getRunDir(runNum), subdir)

  inputGen = InputGenerator(True) # ordered

  # Extract errors from runs
  errors = []
  for f in os.listdir(d):
    if not f.startswith('infotaxis') or not f.endswith('.npz'):
      continue

    f = np.load(os.path.join(d, f), allow_pickle=True)
    errors.append(f['trialOutputs'])
    f.close()
  errors = np.array(errors)

  # Compute what mean error is when target is correct
  minErrors = []
  for i in range(500):
    xStar = inputGen.getRandomInput()
    minErrors.append(it_discrete.model.Model(xStar).getLossTrials(xStar, 50))
  minError = np.average(minErrors)

  variances = []
  averages = []
  for i in range(len(errors[0])):
    variances.append(np.sqrt(np.var(errors[:,i])))
    averages.append(np.average(errors[:,i]))

  fig, ax = plt.subplots()
  ax.errorbar(list(range(len(errors[0]))), averages, color=TRIAL_COLOR, yerr=variances, label='Infotaxis error')
  ax.plot([minError for _ in range(len(errors[0]))], color=TARGET_COLOR, label='Minimum error')
  ax.set_xlabel('Learning step')
  ax.set_ylabel(r'Error $y({\bf x_t})$')
  ax.legend()

  plt.title('Average error during learning')
  if saveName:
    plt.savefig(saveName)
  else:
    plt.show()

def makeEntropyCompPlot(runNum, subdir1, subdir2):
  fig, ax = plt.subplots()
  makeAveragedEntropyPlot(ax, runNum, subdir1, POST_COLOR, 'Entropy (infotaxis policy)')
  makeAveragedEntropyPlot(ax, runNum, subdir2, TRIAL_COLOR, 'Entropy (random policy)')

  ax.set_xlabel('Learning step')
  ax.set_ylabel('Entropy')
  ax.legend()
  plt.title('Average entropy during learning')
  plt.show()

def makeEntropyPlot(runNum, subdir, saveName=None):
  fig, ax = plt.subplots()
  makeAveragedEntropyPlot(ax, runNum, subdir, TRIAL_COLOR, 'Entropy')

  ax.set_xlabel('Learning step')
  ax.set_ylabel('Entropy')
  ax.legend()
  plt.title('Average entropy during learning')
  if saveName:
    plt.savefig('entropy_plot.png')
  else:
    plt.show()

def makeAveragedEntropyPlot(ax, runNum, subdir, color, label):
  d = os.path.join(it_discrete.util.getRunDir(runNum), subdir)

  inputGen = InputGenerator(True) # ordered

  # Extract errors from runs
  ents = []
  for f in os.listdir(d):
    if not f.startswith('infotaxis') or not f.endswith('.npz'):
      continue

    f = np.load(os.path.join(d, f), allow_pickle=True)
    ents.append(f['entropy'])
    f.close()
  ents = np.array(ents)

  variances = []
  averages = []
  for i in range(len(ents[0])):
    variances.append(np.sqrt(np.var(ents[:,i])))
    averages.append(np.average(ents[:,i]))

  return ax.errorbar(list(range(len(ents[0]))), averages, color=color, yerr=variances, label=label)

if __name__ == '__main__':
  runNum = int(sys.argv[1])
  subDir = sys.argv[2]

  #learnFile = it_discrete.util.getInfotaxisFile(runNum)
  #printRunInfo(learnFile)
  #makeAveragedErrorPlot(runNum, subDir)
  #makeEntropyCompPlot(runNum, sys.argv[2], sys.argv[3])
  makeEntropyPlot(runNum, subDir, 'entropy_plot.png')
  makeAveragedErrorPlot(runNum, subDir, 'error_plot.png')

