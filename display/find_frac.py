from it_discrete.constants import *
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation
import numpy as np
import it_discrete.loss
import it_discrete.util
from it_discrete.input import InputGenerator
import it_discrete.model
from scipy.stats import norm
import sys, os


def findFrac(runNum, subdir):
  d = os.path.join(it_discrete.util.getRunDir(runNum), subdir)

  inputGen = InputGenerator(True) # ordered
  count = 0
  frac = None
  print(d)
  for f in os.listdir(d):
    if not f.startswith('infotaxis') or not f.endswith('.npz'):
      continue

    count += 1
    f = np.load(os.path.join(d, f), allow_pickle=True)
    inputs = f['trialInputs']
    xStar = f['xStar']
    if frac is None:
      frac = np.zeros(len(inputs))
    for i, o in enumerate(inputs):
      if ((o == xStar).all()):
        frac[i] += 1.0

  frac /= count
  plt.ylim(0, 1)

  f = open('run_success.csv', 'w')
  f.write('%s,%s\n' % ('step', 'fraction of runs at target'))
  for i,fr in enumerate(frac):
    f.write('%d, %f\n' % (i, fr))
  f.close()

  plt.title('Prob finding target (any move)')
  plt.ylabel('P(success)')
  plt.xlabel('Number of steps')

  plt.plot(frac)
  plt.show()

if __name__ == '__main__':
  findFrac(int(sys.argv[1]), sys.argv[2])







