from it_discrete.constants import *
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation
import numpy as np
import it_discrete.loss
import it_discrete.util
from it_discrete.input import InputGenerator
import it_discrete.model
from scipy.stats import norm
import sys, os
import matplotlib.gridspec as gridspec
plt.style.use(['./mystyle.mplstyle'])

PRIOR_FACTOR = 1.0
PLOT_OFFSET = 0.5
SPIKE_V_OFFSET = 2.2
TARGET_COLOR = 'black'
TRIAL_COLOR = 'tab:green'
POST_COLOR = 'tab:blue'
ENTR_COLOR = 'tab:olive'


def printRunInfo(outputName):
  f = np.load(outputName, allow_pickle=True)

  xStar = f['xStar']
  nSteps = f['nSteps'].item()
  trialInputs = f['trialInputs']
  trialOutputs = f['trialOutputs']
  entropyMaps = f['entropyMaps']
  priorMaps = f['priorMaps']

  capturing = f['capturing'].item()
  nearestNeighbor = f['nearestNeighbor'].item()
  ordering = f['ordering'].item()
  randomMoves = f['randomMoves'].item()

  print('Infotaxis learning run information:')
  print('\txStar: %s' % str(xStar))
  print('\tnSteps: %d' % nSteps)
  print('\tcapturing: %s' % str(capturing))
  print('\tnearest neighbor: %s' % str(nearestNeighbor))
  print('\tordering: %s' % str(ordering))
  print('\trandom moves: %s' % str(randomMoves))

def drawSpike(axis, t, color, offset=0.0):
  sigma = 0.2
  times = np.linspace(t - 3*sigma, t + 3*sigma,100)
  return axis.plot(times, norm.pdf(times, t, sigma)+offset, color=color)

def errorPlot(outputName):
  f = np.load(outputName, allow_pickle=True)

  xStar = f['xStar']
  nSteps = f['nSteps'].item()
  trialInputs = f['trialInputs']
  trialOutputs = f['trialOutputs']

  # Compute mean error for correct target
  m = it_discrete.model.Model(xStar)
  errors = m.getLossTrials(xStar, 500)
  minError = np.average(errors)
  
  plt.plot(trialOutputs, color=TRIAL_COLOR, label='trial error')
  plt.plot([minError for _ in range(len(trialOutputs))], color=TARGET_COLOR, label='min error')
  plt.show()

def movies(outputName, saveName=None):
  """
  Makes movie showing prior distribution for various targets as well as inputs over time.
  """
  f = np.load(outputName, allow_pickle=True)

  xStar = f['xStar']
  nSteps = f['nSteps'].item()
  trialInputs = f['trialInputs']
  trialOutputs = f['trialOutputs']
  entropyMaps = f['entropyMaps']
  priorMaps = f['priorMaps']


  fig = plt.figure(tight_layout=True)
  gs = gridspec.GridSpec(6, 2)

  ax1 = fig.add_subplot(gs[0:2, 0])
  ax2 = fig.add_subplot(gs[2:4, 0])
  ax3 = fig.add_subplot(gs[4:6, 0])
  ax4 = fig.add_subplot(gs[0:3, 1])
  ax5 = fig.add_subplot(gs[3:6, 1])  

  keys = list(priorMaps[4].keys())
  keyIndex = {keys[i]: i for i in range(len(keys))}
  priors = [
    [priorMap[k] for k in keys]
    for priorMap in priorMaps
  ]
  entropies = [
    [entropy[k] if k in entropy else 0.0 for k in keys]
    for entropy in entropyMaps
  ]
  trials = [keyIndex[tuple(x)] for x in trialInputs]
  correct = keyIndex[tuple(xStar)]
  
  minPrior = min([min(pr) for pr in priors])
  maxPrior = max([max(pr) for pr in priors])
  minEntropy = min([min(e) for e in entropies])
  maxEntropy = max([max(e) for e in entropies])
  maxPrior = (maxPrior - minPrior) * 1.1 + minPrior
  maxEntropy = (maxEntropy - minEntropy) * 1.1 + minEntropy

  x1Data = list(range(len(keys)))
  #def init():
  #  return update(0)

  axs = [ax1, ax2, ax3, ax4, ax5]

  def init():
    actors = update(0)
    plt.tight_layout()

    return actors

  def update(frame):
    for ax in axs:
      ax.cla()
    for ax in [ax1, ax2, ax3]:
      ax.set_xlim(-0.5, len(keys)+0.5)
      ax.set_xticks([len(keys)])
    for ax in [ax4, ax5]:
      ax.set_xlim(-0.5, 50.5)
      
      ax.set_yticks([])

    ax3.set_xlabel(r'Search space ${\bf x}$')
    ax5.set_xlabel(r'Spike times ($s$)')
      
    ax1.set_ylim(0.0, 1.0)
    ax2.set_ylim(minPrior, maxPrior)
    ax3.set_ylim(minEntropy, maxEntropy)
    ax1.set_yticks([])

    ax1.set_title('Trial/Target')
    ax2.set_title('Posterior')
    ax2.set_ylabel(r'Posterior $p({\bf x^*})$')
    ax3.set_title('Entropy for possible input')
    ax3.set_ylabel(r'Entropy $\langle S({\bf x_t})\rangle$')
    ax4.set_title('Trial spikes')
    ax5.set_title('Target spikes')

    # ax1.cla()
    # ax1.set_xlim(-0.5, len(keys)+0.5)
    # ax1.set_ylim(0.0, 1.5)
    # ax1.set_xlabel(r'Search space ${\bf x}$')
    # ax1.set_xticks([])
    # ax1.set_ylabel(r'Posterior distribution $p({\bf x^*})$')
    # ax1.set_ylabel(r'Expected entropy for move $\langle S({\bf x_t})\rangle$')
    
    # ax2.cla()
    # ax2.set_xlim(-0.5, 50.5)
    # ax2.set_xlabel(r'Spike times ($s$)')
    # ax2.set_yticks([])
    
    actors = update2(frame)
    #ax1.legend(loc='upper left')
    #ax2.legend(loc='upper left')

    return actors


  def update2(frame):
    actors = []
    ln1, = ax2.plot(x1Data, priors[frame], linestyle='None', color=POST_COLOR, label=r'$p_t({\bf x^*})$ (posterior)', marker='s')
    ln2, = ax3.plot(x1Data, entropies[frame], color='yellow', label=r'$\langle S({\bf x})\rangle$ (entropy)')
    lnt, = ax1.plot([trials[frame]], 0.5, color=TRIAL_COLOR, label=r'${\bf x}_t$ (trial)', marker='s', markersize=15.0)
    lns, = ax1.plot([correct], 0.5, color=TARGET_COLOR, label=r'${\bf x^*}$ (target)', marker='*', markersize=15.0)
    actors = [ln1, ln2, lnt, lns]

    for i,xT in enumerate(trialInputs[frame]):
      actors.append(drawSpike(ax4, TIMES[xT], TRIAL_COLOR, offset=SPIKE_V_OFFSET)[0])

    for i,xT in enumerate(xStar):
      actors.append(drawSpike(ax5, TIMES[xT], TARGET_COLOR)[0])

    return actors

  fig.set_dpi(300)
  fig.set_size_inches(11.2, 7.0)

  ani = FuncAnimation(fig, update, frames=range(nSteps),
                      init_func=init, blit=True, interval=1000)
  #ani.save('movie.mp4')
  #Writer = animation.writers['ffmpeg']
  #writer = Writer(fps=4, metadata=dict(artist='Me'), bitrate=1800)
  if saveName:
    ani.save(saveName)
  else:
    plt.show()

def movieEntropy(outputName):
  """
  Makes movie showing expected Entropy for various moves over time.
  """
  f = np.load(outputName, allow_pickle=True)

  xStar = f['xStar']
  nSteps = f['nSteps'].item()
  trialInputs = f['trialInputs']
  trialOutputs = f['trialOutputs']
  priorMaps = f['priorMaps']
  moveEntropies = f['moveEntropies'] # list of maps mapping [index, direction] to expected entropy
  actualMoves = f['moves']

  possibleMoves = list(moveEntropies[0].keys())
  movesToIndex = {possibleMoves[i]: i for i in range(len(possibleMoves))}

  actualMoves = [movesToIndex[tuple(m)] for m in actualMoves]
  moveEntropies = [[entropies[m] for m in possibleMoves] for entropies in moveEntropies]
  for ents in moveEntropies:
    for i in range(len(ents)):
      if ents[i] is None:
        ents[i] = 0.0

  fig, ax = plt.subplots()
  
  ln1 = plt.plot(range(len(possibleMoves)), moveEntropies[0])
  ln2 = plt.axvline(x=actualMoves[0])
  def init():
    ax.cla()
    ax.set_xlim(-0.5, 8.5)
    ln1, = plt.plot(range(len(possibleMoves)), moveEntropies[0])
    ln2 = plt.axvline(x=actualMoves[0])
    return [ln1,ln2]

  def update(frame):
    ln1, = plt.plot(range(len(possibleMoves)), moveEntropies[frame])
    ln2 = plt.axvline(x=actualMoves[frame])
    return [ln1,ln2]

  ani = FuncAnimation(fig, update, frames=range(nSteps),
                      init_func=init, blit=True, interval=500)
  plt.show()

def priorMovie(outputName):
  """
  Makes movie showing prior distribution for various targets as well as inputs over time.
  """
  f = np.load(outputName, allow_pickle=True)

  xStar = f['xStar']
  nSteps = f['nSteps'].item()
  trialInputs = f['trialInputs']
  trialOutputs = f['trialOutputs']
  entropyMaps = f['entropyMaps']
  priorMaps = f['priorMaps']

  keys = list(priorMaps[4].keys())
  keyIndex = {keys[i]: i for i in range(len(keys))}
  priors = [
    [priorMap[k] for k in keys]
    for priorMap in priorMaps
  ]
  entropies = [
    [0.5*entropy[k] if k in entropy else 0.0 for k in keys]
    for entropy in entropyMaps
  ]
  trials = [keyIndex[tuple(x)] for x in trialInputs]
  correct = keyIndex[tuple(xStar)]
  
  fig, ax = plt.subplots()
  xData = list(range(len(keys)))
  yData = priors[0]
  ln, = plt.plot(xData, yData)

  def init():
    ax.cla()
    ax.set_xlim(-0.5, 70.5)
    ln.set_data(xData, yData)
    return ln,

  def update(frame):
    ln1, = plt.plot(xData, priors[frame], color='orange', marker='s', line='None')
    ln2, = plt.plot(xData, entropies[frame], color='green', marker='s', line='None')
    lnt = plt.axvline(x=trials[frame], color='red')
    lns = plt.axvline(x=correct, color='blue')
    print('%s: %f' % (str(trialInputs[frame]), entropyMaps[frame][tuple(trialInputs[frame])]))

    return [ln1, ln2, lnt, lns]

  ani = FuncAnimation(fig, update, frames=range(nSteps),
                      init_func=init, blit=True, interval=1000)
  plt.show()


def motorMovie(outputName):
  """
  Makes movie showing trial spikes over time.
  """
  f = np.load(outputName, allow_pickle=True)

  xStar = f['xStar']
  nSteps = f['nSteps'].item()
  trialInputs = f['trialInputs']
  trialOutputs = f['trialOutputs']
  entropyMaps = f['entropyMaps']
  priorMaps = f['priorMaps']

  xStarTimes = [TIMES[t] for t in xStar]
  xTimes = [[TIMES[t] for t in xInds] for xInds in trialInputs]

  fig, ax = plt.subplots()
  correctSpikes = [plt.axvline(x=(t+PLOT_OFFSET), color='blue') for t in xStarTimes]
  trialSpikes = [plt.axvline(x=(t-PLOT_OFFSET), color='red') for t in xStarTimes]

  def init():
    ax.cla()
    ax.set_xlim(-1.0, 51.0)
    ax.set_ylim(0.0, 1.0)
    return correctSpikes + trialSpikes

  def update(frame):
    trialSpikes = [plt.axvline(x=(t-PLOT_OFFSET), color='red') for t in xTimes[frame]]
    return correctSpikes + trialSpikes

  ani = FuncAnimation(fig, update, frames=range(nSteps),
                      init_func=init, blit=True, interval=500)
  plt.show()



if __name__ == '__main__':
  runNum = int(sys.argv[1])
  suffix = sys.argv[2]
  if len(sys.argv) == 4:
    saveName = sys.argv[3]
  else:
    saveName = None
  learnFile = it_discrete.util.getInfotaxisFile(runNum, suffix=suffix)
  printRunInfo(learnFile)
  movies(learnFile, saveName=saveName)
  
