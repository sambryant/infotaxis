import matplotlib.pyplot as plt
import os, sys, time
import numpy as np
import it_discrete.loss
from it_discrete.loss import NUM_LOSS_BINS
import it_discrete.util
import it_discrete.constants
from it_discrete.input import InputGenerator

CUTOFF = 0

class PDFBuilder2(object):

  def __init__(self, ordering):
    self.ordering = ordering
    self.probMap = {} # map from (x1,x2,x3,x4) to 2D array indexed by [yBin, xStarIndex]
    self.inputGen = InputGenerator(ordering)

    for x in self.inputGen.getInputs():
      self.probMap[x] = np.zeros((NUM_LOSS_BINS, len(self.inputGen.getInputs())))

  def addData(self, xInds, xStarInds, yValues):
    dp = 1.0/len(yValues)
    xSI = self.inputGen.inputIndex(xStarInds)
    yValues += np.random.normal(scale=it_discrete.constants.LOSS_NOISE, size=len(yValues))
    yBins = (np.floor(it_discrete.loss.NUM_LOSS_BINS * yValues)).astype(int)

    for i,y in enumerate(yBins):
      self.probMap[xInds][y, xSI] += dp

  def addDataFromFile(self, fname):
    d = np.load(fname, allow_pickle=True)

    sampledResults = d['sampledResults'].item()

    for (x1,x2,x3,x4,xS1,xS2,xS3,xS4), yValues in sampledResults.items():
      self.addData((x1, x2, x3, x4), (xS1, xS2, xS3, xS4), yValues)
    d.close()

  def saveToFile(self, fname):
    np.save(fname, self.probMap)
    np.save(fname+'ordering', self.ordering)
    #np.savez(fname, probMap=self.probMap, ordering=self.ordering)
    print('saved %s' % os.path.abspath(fname))

class Pdf2(object):
  
  @classmethod
  def loadFromFile(cls, pdfFile, ordering):
    pMap = np.load(pdfFile, allow_pickle=True).item()
    #pMap = d['probMap'].item()
    #ordering = d['ordering']
    return Pdf2(pMap, ordering)

  def __init__(self, probMap, ordering):
    self.ordering = ordering
    self.inputGen = InputGenerator(ordering)
    self._probMap = probMap # map from (x1,x2,x3,x4) to 2D array indexed by [yBin, xStarIndex]

    xStars = self.inputGen.getInputs()
    yIndices = it_discrete.loss.Y_INDICES

    self.xStars = xStars
    self.xStarIndicesMap = {}
    for i, xStar in enumerate(xStars):
      self.xStarIndicesMap[xStar] = i

    self.checkConsistency()

  def checkConsistency(self):
    """
    Checks for each x,xStar that sum_y p(y|x,xStar) = 1
    """
    for x in self.inputGen.getInputs():
      probs = np.sum(self._probMap[x], axis=0)
      for i,p in enumerate(probs):
        if np.abs(p-1.0) > 0.001:
          print('Found inconsistent prob sum')
          print('x : %s' % str(x))
          print('x*: %s' % str(self.xStars[i]))
          print('pY: %f' % p)
          raise Exception()

  def p(self, x, xStar, y):
    return self._probMap[x][y, self.xStarIndicesMap[xStar]]

  def getProb(self, xInds, xStarInds, yBin):
    return self.p(xInds, xStarInds, yBin)

def makeDists(runNum):
  sampleDir = it_discrete.util.getDataDir(runNum)
  destDir = it_discrete.util.getDistDir(runNum)
  files = os.listdir(sampleDir)
  files = [os.path.join(sampleDir, f) for f in files]
  print('Digesting %d files' % len(files))
  for i,f in enumerate(files):
    if not f.endswith('.npz'):
      continue

    d = np.load(f, allow_pickle=True)
    sampledResults = d['sampledResults'].item()
    for (x1,x2,x3,x4,xS1,xS2,xS3,xS4), ys in sampledResults.items():
      plt.clf()
      plt.hist(ys, bins=20)
      plt.savefig('%s/dist_x%d.%d.%d.%d_xS%d.%d.%d.%d.png' % (destDir, x1,x2,x3,x4,xS1,xS2,xS3,xS4))
    d.close()

def showCorrectYHist(sampleDir):
  yValues = []
  files = os.listdir(sampleDir)
  files = [os.path.join(sampleDir, f) for f in files]
  print('Digesting %d files' % len(files))
  for i,f in enumerate(files):
    if not f.endswith('.npz'):
      continue

    d = np.load(f, allow_pickle=True)
    sampledResults = d['sampledResults'].item()
    for (x1,x2,x3,x4,xS1,xS2,xS3,xS4), ys in sampledResults.items():
      if x1 != xS1 or x2 != xS2 or x3 != xS3 or x4 != xS4:
        continue
      yValues.extend(ys)
    d.close()

  plt.hist(yValues, bins=20)
  plt.show()

def showYHist(sampleDir):
  yValues = []
  files = os.listdir(sampleDir)
  files = [os.path.join(sampleDir, f) for f in files]
  print('Digesting %d files' % len(files))
  for i,f in enumerate(files):
    if not f.endswith('.npz'):
      continue

    d = np.load(f, allow_pickle=True)
    sampledResults = d['sampledResults'].item()
    for (x1,x2,x3,x4,xS1,xS2,xS3,xS4), ys in sampledResults.items():
      yValues.extend(ys)
    d.close()

  plt.hist(yValues, bins=100)
  plt.show()

def showYBinHist(sampleDir):
  yValues = []
  files = os.listdir(sampleDir)
  files = [os.path.join(sampleDir, f) for f in files]
  print('Digesting %d files' % len(files))
  for i,f in enumerate(files):
    if not f.endswith('.npz'):
      continue

    d = np.load(f, allow_pickle=True)
    sampledResults = d['sampledResults'].item()
    for (x1,x2,x3,x4,xS1,xS2,xS3,xS4), ys in sampledResults.items():
      if (x1, x2, x3, x4) != (5,5,2,5) or (xS1,xS2,xS3,xS4) != (6,7,6,1):
        continue
      for y in ys:
        yBin = it_discrete.loss.getLossBin(y)
        yValues.append(yBin)

    d.close()

  plt.hist(yValues, bins=100)
  plt.show()

def reconstructPdf(runNum, ordering):
  sampleDir = it_discrete.util.getDataDir(runNum)
  pdfFile = it_discrete.util.getPdfFile(runNum)


  builder = PDFBuilder2(ordering)
  files = os.listdir(sampleDir)
  files = [os.path.join(sampleDir, f) for f in files]
  print('Building PDF from files in %s' % sampleDir)
  print('Digesting %d files' % len(files))
  t0 = time.time()
  for i,f in enumerate(files):
    if not f.endswith('.npz'):
      continue
    if CUTOFF and i == CUTOFF:
      break
    builder.addDataFromFile(f)
    it_discrete.util.printProgressBar(i+1, CUTOFF or len(files), suffix='time: %f' % (time.time() - t0))
    sys.stdout.flush()

  builder.saveToFile(pdfFile)


if __name__ == '__main__':
  runNum = int(sys.argv[1])
  #showYBinHist(it_discrete.util.getDataDir(runNum))
  #makeDists(runNum)
  reconstructPdf(runNum, False)
  # runNum = int(sys.argv[1])
  # print('Loading PDF...')
  # sys.stdout.flush()
  #pdf = Pdf2.loadFromFile(it_discrete.util.getPdfFile(runNum))
  # #pdf2 = Pdf2(pdf, ordering)
  #print(pdf.getProb( (0,1,2,3), (0,1,2,3), 0))


