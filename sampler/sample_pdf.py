import sys, os, itertools, time
import numpy as np
from it_discrete.constants import *
import it_discrete.loss
import it_discrete.noise
import it_discrete.model
import it_discrete.util
from it_discrete.input import InputGenerator

def getSamples(xInds, xStarInds, nSamples):
  """
  Returns array of the bin index of the error output for the given input/template over several trials.
  """
  m = it_discrete.model.Model(xStarInds)

  return m.getLossTrials(xInds, nSamples)

def getSamplesOld(xInds, xStarInds, nSamples):
  """
  Returns array of the bin index of the error output for the given input/template over several trials.
  """
  m = it_discrete.model.Model(xStarInds)

  return [m.getLoss(xInds) for i in range(nSamples)]
  
def generateSamples(ordered, nSamples, x1, x2, x3, outputDir):
  """
  Generates all samples for fixed (x1, x2, x3) and stores to same file.
  """
  print('Generating ordered samples: %s' % str(ordered))
  inputGen = InputGenerator(ordered)

  if ordered:
    if not (x3 > x2 and x2 > x1):
      raise Exception('Indices of spikes must be in increasing order')


  # Check if output file already exists
  fName = os.path.join(outputDir, '%s_samples_N%d_x1=%d_x2=%d_x3=%d.npz' % ('ordered' if ordered else 'unordered', nSamples, x1, x2, x3))
  if os.path.exists(fName):
    print('Target output file already exists: %s' % fName)
    return

  # Create list of all possible input/targets
  inputs = []
  x4Range = range(N_TIMES) if not ordered else range(x3+1, N_TIMES)
  for x4 in x4Range:
    for xS1, xS2, xS3, xS4 in inputGen.getInputs():
      inputs.append([x1, x2, x3, x4, xS1, xS2, xS3, xS4])

  # For each possible input/target combination, get samples
  print('Generating samples for %d input combinations' % len(inputs))
  sampledResults = {} # Map from (x1,x2,x3,x4,xS1,xS2,xS3,xS4) to array of samples.
  for x1,x2,x3,x4,xS1,xS2,xS3,xS4 in inputs:
    sampledResults[(x1,x2,x3,x4,xS1,xS2,xS3,xS4)] = getSamples((x1,x2,x3,x4), (xS1,xS2,xS3,xS4), nSamples)
    
  np.savez(fName, nSamples=nSamples, x1=x1, x2=x2, x3=x3, sampledResults=sampledResults)
  print('Saved numpy file %s' % fName)

USAGE = '%s <ordered|unordered> <run num> <num samples> <x1> <x2> <x3>'

if __name__ == '__main__':

  if len(sys.argv) != 7:
    print('Bad usage: expected 7 args')
    print('Usage: %s' % (USAGE % sys.argv[0]))
    sys.exit(-1)
  else:
    if sys.argv[1] == 'ordered':
      ordered = True
    elif sys.argv[1] == 'unordered':
      ordered = False
    else:
      raise Exception('Expected first arg to be "ordered" or "unordered"')
    runNum = int(sys.argv[2])
    nSamples = int(sys.argv[3])
    x1 = int(sys.argv[4])
    x2 = int(sys.argv[5])
    x3 = int(sys.argv[6])
    outputDir = it_discrete.util.getDataDir(runNum)
    generateSamples(ordered, nSamples, x1, x2, x3, outputDir)
