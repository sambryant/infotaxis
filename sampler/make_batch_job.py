import json, os, stat,itertools,sys

import it_discrete.constants
import it_discrete.util

RAM = '8G'
PARTITION = 'day'
TIME = '24:00:00'   # maximum time on grace node before job is killed
CONDA_ENV = 'myenv-ordered' # conda environment to use when executing code

def makeBatchJob(
  ordered,
  runNum,
  nSamples,
  x1,
  x2, 
  x3, 
  runOnGrace=True,
  ram=RAM,
  partition=PARTITION,
  time=TIME,
  condaEnv=CONDA_ENV):

  jobname='%d.X%d.%d.%d' % (nSamples, x1, x2, x3)
  name = '%s_N%d_%d_%d_%d.sh' % ('ordered' if ordered else 'unordered', nSamples, x1, x2, x3)
  jobdir = it_discrete.util.getJobDir(runNum)
  outdir = it_discrete.util.getDataDir(runNum)

  scriptFile = os.path.join(jobdir, name)
  f = open(scriptFile, 'w')
  f.write('#!/bin/bash\n')
  f.write('\n')

  if runOnGrace:
    # Slurm metadata
    f.write('#SBATCH --partition=%s\n' % partition)
    f.write('#SBATCH --job-name="%s"\n' % jobname)
    f.write('#SBATCH --ntasks=1 --nodes=1\n')
    f.write('#SBATCH --mem-per-cpu=%s\n' % ram)
    f.write('#SBATCH --time=%s\n' % time)
    f.write('#SBATCH --mail-type=FAIL\n')
    f.write('#SBATCH --mail-user=samuel.bryant@yale.edu\n')
    f.write('\n')

    # Tell grace which modules to load
    f.write('module load Langs/Python/3.6-anaconda\n')
    f.write('\n')

    # Load proper anaconda environment
    f.write('source activate %s\n' % condaEnv)
    f.write('\n')
  else:
    pass
    #f.write('cd /home/sam/workspace/it_discrete_ordered\n')
    #f.write('pipenv shell')

  f.write('python -m "it_discrete.sampler.sample_pdf" "%s" "%d" "%d" "%d" "%d" "%d"\n' % (
    'ordered' if ordered else 'unordered',
    runNum, nSamples, x1, x2, x3))

  # If job is local, make it executable
  if not runOnGrace:
    st = os.stat(scriptFile)
    os.chmod(scriptFile, st.st_mode | stat.S_IEXEC)

  print('Created script "%s"' % scriptFile)

if __name__ == '__main__':
  ordered = sys.argv[1]
  runNum = int(sys.argv[2])
  nSamples = int(sys.argv[3])

  inputs = []
  if ordered == 'ordered':
    isOrdered = True
    for x1 in range(it_discrete.constants.N_TIMES-3):
      for x2 in range(x1+1, it_discrete.constants.N_TIMES-2):
        for x3 in range(x2+1, it_discrete.constants.N_TIMES-1):
          inputs.append((x1,x2,x3))
  elif ordered == 'unordered':
    isOrdered = False
    for x1 in range(it_discrete.constants.N_TIMES):
      for x2 in range(it_discrete.constants.N_TIMES):
        for x3 in range(it_discrete.constants.N_TIMES):
          inputs.append((x1,x2,x3))
  else:
    raise Exception('Expected first argument to be "ordered" or "unordered"')

  for x1, x2, x3 in inputs:
    makeBatchJob(isOrdered, runNum, nSamples, x1, x2, x3)
